import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;

import model.Arc;
import model.Graphe;
import model.ModelFactory;
import model.Relation;
import model.Sommet;
import model.impl.ModelFactoryImpl;

public class test {

	public static void main(String[] args) throws IOException {

		// Initialsiation du mod�le et cr�ation du graph
		String path = "model/modelTest1.uml";
		String targetName ="graph1";
		writeGraphe(path, targetName);
		String path2 = "model/modelTest2.uml";
		String targetName2 ="graph2";
		writeGraphe(path2, targetName2);
	}

	public static void writeGraphe(String path, String targetName) throws IOException {
		// Chargement des donn�es
		Resource resource = chargerModele(path, UMLPackage.eINSTANCE);
		if (resource == null)
			System.err.println(" Erreur de chargement du mod�le");
		Model toGraph = (Model) resource.getContents().get(0);
		ModelFactory modelFacto = ModelFactoryImpl.init();
		String nomModele = toGraph.getName();
		Graphe graphe = modelFacto.createGraphe();

		// Rcup�ration et remplissage
		ArrayList<Package> packages = getAllPackage(toGraph);
		Package p = getPackageByName(packages, "P1");
		ArrayList<Class> classes = getClass(p);
		fillSommet(classes, modelFacto, graphe);
		fillArc(classes, modelFacto, graphe);

		// Ecriture au format texte
		File f = new File("grapheTest.txt");
		FileWriter fw = new FileWriter(f);
		fw.write("Liste des sommets :");

		for (Sommet s : graphe.getSommet()) {
			fw.write("\n" + s.getName());
		}

		fw.write("\n\nListe des arcss :");
		for (Arc a : graphe.getArc()) {
			fw.write("\n Origine :" + a.getOrigine().getName() + " ; Destination : " + a.getDestination().getName()
					+ " ; Type :" + a.getRelation().getName());
		}

		fw.close();

		// Ecriture au format dot
		File f2 = new File(targetName+".dot");
		FileWriter fw2 = new FileWriter(f2);
		fw2.write("digraph mon_graphe {");
		for (Arc a : graphe.getArc()) {
			fw2.write("\n" + a.getOrigine().getName() + " -> " + a.getDestination().getName()+";");
		}
		fw2.write("\n}");
		fw2.close();
		
		System.out.println("Ecriture termin�e !");
	}

	public static ArrayList<Package> getAllPackage(PackageableElement m) {
		EList<PackageableElement> listOfPackaged = ((Package) m).getPackagedElements();
		ArrayList<Package> toRetrieve = new ArrayList<Package>();
		for (PackageableElement elem : listOfPackaged) {
			if (elem instanceof Package) {
				toRetrieve.add((Package) elem);
				toRetrieve.addAll(getAllPackage(elem));
			}
		}
		return toRetrieve;
	}

	public static Package getPackageByName(ArrayList<Package> m, String name) {
		Package toRetrieve = null;
		for (Package p : m) {
			if (p.getName().equals(name)) {
				toRetrieve = p;
			}
		}
		return toRetrieve;
	}

	public static Class getClassByName(Package p, String name) {
		Class c = null;
		for (PackageableElement elem : p.getPackagedElements()) {
			if (elem.getName().equals(name) && elem instanceof Class) {
				c = (Class) elem;
			}
		}
		return c;
	}

	public static ArrayList<Class> getClass(Package p) {
		ArrayList<Class> toRetrieve = new ArrayList<>();
		for (PackageableElement elem : p.getPackagedElements()) {
			if (elem instanceof Class) {
				toRetrieve.add((Class) elem);
			}
		}
		return toRetrieve;
	}

	public static ArrayList<Class> getSuperclassByClass(Class c) {
		ArrayList<Class> toRetrieve = new ArrayList<>();
		for (Class elem : c.getSuperClasses()) {
			if (elem instanceof Class) {
				toRetrieve.add((Class) elem);
			}
		}
		return toRetrieve;
	}

	public static HashMap<String, AggregationKind> getRelatedClass(Class c) {
		HashMap<String, AggregationKind> related = new HashMap<>();
		ArrayList<Property> p = new ArrayList<>();
		for (Property elem : c.getOwnedAttributes()) {
			;
			if (elem.getAggregation() != null) {
				related.put(elem.getName(), elem.getAggregation());
			}
		}
		return related;
	}

	public static void fillSommet(ArrayList<Class> sommet, ModelFactory modelFacto, Graphe graphe) {
		for (Class c : sommet) {
			Sommet s = modelFacto.createSommet();
			s.setName(c.getName());
			graphe.getSommet().add(s);
		}
	}

	public static void fillArc(ArrayList<Class> sommet, ModelFactory modelFacto, Graphe graphe) {
		for (Class elem : sommet) {

			HashMap<String, AggregationKind> relation = getRelatedClass(elem);
			ArrayList<Class> heritages = getSuperclassByClass(elem);

			for (Map.Entry<String, AggregationKind> entry : relation.entrySet()) {
				Arc arc = modelFacto.createArc();
				arc.setOrigine(getSommetByName(graphe, elem.getName()));
				arc.setDestination(getSommetByName(graphe, entry.getKey().toUpperCase()));

				if ("shared".equals(entry.getValue().getName())) {
					arc.setRelation(Relation.AGGREGATION);
				}
				if ("composite".equals(entry.getValue().getName())) {
					arc.setRelation(Relation.COMPOSITION);
				}
				if ("none".equals(entry.getValue().getName())) {
					arc.setRelation(Relation.ASSOCIATION);
				}
				graphe.getArc().add(arc);
			}

			for (Class superC : heritages) {
				Arc arc = modelFacto.createArc();
				arc.setOrigine(getSommetByName(graphe, elem.getName()));
				arc.setDestination(getSommetByName(graphe, superC.getName()));
				arc.setRelation(Relation.HERITAGE);
				graphe.getArc().add(arc);
			}
		}
	}

	public static Sommet getSommetByName(Graphe graphe, String name) {
		EList<Sommet> sommets = graphe.getSommet();
		Sommet s = null;
		for (Sommet slook : sommets) {
			if (name.contentEquals(slook.getName())) {
				s = slook;
			}
		}
		return s;
	}

	public static void sauverModele(String uri, EObject root) {
		Resource resource = null;
		try {
			URI uriUri = URI.createURI(uri);
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
			resource = (new ResourceSetImpl()).createResource(uriUri);
			resource.getContents().add(root);
			resource.save(null);
		} catch (Exception e) {
			System.err.println("ERREUR sauvegarde du modèle : " + e);
			e.printStackTrace();
		}
	}

	public static Resource chargerModele(String uri, EPackage pack) {
		Resource resource = null;
		try {
			URI uriUri = URI.createURI(uri);
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("uml", new XMIResourceFactoryImpl());
			resource = (new ResourceSetImpl()).createResource(uriUri);
			XMLResource.XMLMap xmlMap = new XMLMapImpl();
			xmlMap.setNoNamespacePackage(pack);
			java.util.Map options = new java.util.HashMap();
			options.put(XMLResource.OPTION_XML_MAP, xmlMap);
			resource.load(options);
		} catch (Exception e) {
			System.err.println("ERREUR chargement du modèle : " + e);
			e.printStackTrace();
		}
		return resource;
	}
}

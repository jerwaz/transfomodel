/**
 */
package model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.Arc#getRelation <em>Relation</em>}</li>
 *   <li>{@link model.Arc#getDestination <em>Destination</em>}</li>
 *   <li>{@link model.Arc#getOrigine <em>Origine</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getArc()
 * @model
 * @generated
 */
public interface Arc extends EObject {
	/**
	 * Returns the value of the '<em><b>Relation</b></em>' attribute.
	 * The literals are from the enumeration {@link model.Relation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' attribute.
	 * @see model.Relation
	 * @see #setRelation(Relation)
	 * @see model.ModelPackage#getArc_Relation()
	 * @model
	 * @generated
	 */
	Relation getRelation();

	/**
	 * Sets the value of the '{@link model.Arc#getRelation <em>Relation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation</em>' attribute.
	 * @see model.Relation
	 * @see #getRelation()
	 * @generated
	 */
	void setRelation(Relation value);

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link model.Sommet#getDestinationedBy <em>Destinationed By</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' reference.
	 * @see #setDestination(Sommet)
	 * @see model.ModelPackage#getArc_Destination()
	 * @see model.Sommet#getDestinationedBy
	 * @model opposite="destinationedBy" required="true"
	 * @generated
	 */
	Sommet getDestination();

	/**
	 * Sets the value of the '{@link model.Arc#getDestination <em>Destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination</em>' reference.
	 * @see #getDestination()
	 * @generated
	 */
	void setDestination(Sommet value);

	/**
	 * Returns the value of the '<em><b>Origine</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link model.Sommet#getOriginedBy <em>Origined By</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origine</em>' reference.
	 * @see #setOrigine(Sommet)
	 * @see model.ModelPackage#getArc_Origine()
	 * @see model.Sommet#getOriginedBy
	 * @model opposite="OriginedBy" required="true"
	 * @generated
	 */
	Sommet getOrigine();

	/**
	 * Sets the value of the '{@link model.Arc#getOrigine <em>Origine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origine</em>' reference.
	 * @see #getOrigine()
	 * @generated
	 */
	void setOrigine(Sommet value);

} // Arc

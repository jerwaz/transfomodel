/**
 */
package model.impl;

import java.util.Collection;

import model.Arc;
import model.ModelPackage;
import model.Sommet;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sommet</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link model.impl.SommetImpl#getName <em>Name</em>}</li>
 *   <li>{@link model.impl.SommetImpl#getDestinationedBy <em>Destinationed By</em>}</li>
 *   <li>{@link model.impl.SommetImpl#getOriginedBy <em>Origined By</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SommetImpl extends MinimalEObjectImpl.Container implements Sommet {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDestinationedBy() <em>Destinationed By</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestinationedBy()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc> destinationedBy;

	/**
	 * The cached value of the '{@link #getOriginedBy() <em>Origined By</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginedBy()
	 * @generated
	 * @ordered
	 */
	protected EList<Arc> originedBy;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SommetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.SOMMET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SOMMET__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Arc> getDestinationedBy() {
		if (destinationedBy == null) {
			destinationedBy = new EObjectWithInverseResolvingEList<Arc>(Arc.class, this, ModelPackage.SOMMET__DESTINATIONED_BY, ModelPackage.ARC__DESTINATION);
		}
		return destinationedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Arc> getOriginedBy() {
		if (originedBy == null) {
			originedBy = new EObjectWithInverseResolvingEList<Arc>(Arc.class, this, ModelPackage.SOMMET__ORIGINED_BY, ModelPackage.ARC__ORIGINE);
		}
		return originedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.SOMMET__DESTINATIONED_BY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDestinationedBy()).basicAdd(otherEnd, msgs);
			case ModelPackage.SOMMET__ORIGINED_BY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOriginedBy()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.SOMMET__DESTINATIONED_BY:
				return ((InternalEList<?>)getDestinationedBy()).basicRemove(otherEnd, msgs);
			case ModelPackage.SOMMET__ORIGINED_BY:
				return ((InternalEList<?>)getOriginedBy()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.SOMMET__NAME:
				return getName();
			case ModelPackage.SOMMET__DESTINATIONED_BY:
				return getDestinationedBy();
			case ModelPackage.SOMMET__ORIGINED_BY:
				return getOriginedBy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.SOMMET__NAME:
				setName((String)newValue);
				return;
			case ModelPackage.SOMMET__DESTINATIONED_BY:
				getDestinationedBy().clear();
				getDestinationedBy().addAll((Collection<? extends Arc>)newValue);
				return;
			case ModelPackage.SOMMET__ORIGINED_BY:
				getOriginedBy().clear();
				getOriginedBy().addAll((Collection<? extends Arc>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.SOMMET__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelPackage.SOMMET__DESTINATIONED_BY:
				getDestinationedBy().clear();
				return;
			case ModelPackage.SOMMET__ORIGINED_BY:
				getOriginedBy().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.SOMMET__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelPackage.SOMMET__DESTINATIONED_BY:
				return destinationedBy != null && !destinationedBy.isEmpty();
			case ModelPackage.SOMMET__ORIGINED_BY:
				return originedBy != null && !originedBy.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //SommetImpl

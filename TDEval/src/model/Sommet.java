/**
 */
package model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sommet</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.Sommet#getName <em>Name</em>}</li>
 *   <li>{@link model.Sommet#getDestinationedBy <em>Destinationed By</em>}</li>
 *   <li>{@link model.Sommet#getOriginedBy <em>Origined By</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getSommet()
 * @model
 * @generated
 */
public interface Sommet extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see model.ModelPackage#getSommet_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link model.Sommet#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Destinationed By</b></em>' reference list.
	 * The list contents are of type {@link model.Arc}.
	 * It is bidirectional and its opposite is '{@link model.Arc#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destinationed By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destinationed By</em>' reference list.
	 * @see model.ModelPackage#getSommet_DestinationedBy()
	 * @see model.Arc#getDestination
	 * @model opposite="destination"
	 * @generated
	 */
	EList<Arc> getDestinationedBy();

	/**
	 * Returns the value of the '<em><b>Origined By</b></em>' reference list.
	 * The list contents are of type {@link model.Arc}.
	 * It is bidirectional and its opposite is '{@link model.Arc#getOrigine <em>Origine</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origined By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origined By</em>' reference list.
	 * @see model.ModelPackage#getSommet_OriginedBy()
	 * @see model.Arc#getOrigine
	 * @model opposite="origine"
	 * @generated
	 */
	EList<Arc> getOriginedBy();

} // Sommet

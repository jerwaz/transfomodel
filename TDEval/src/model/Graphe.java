/**
 */
package model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graphe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link model.Graphe#getSommet <em>Sommet</em>}</li>
 *   <li>{@link model.Graphe#getArc <em>Arc</em>}</li>
 * </ul>
 *
 * @see model.ModelPackage#getGraphe()
 * @model
 * @generated
 */
public interface Graphe extends EObject {
	/**
	 * Returns the value of the '<em><b>Sommet</b></em>' containment reference list.
	 * The list contents are of type {@link model.Sommet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sommet</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sommet</em>' containment reference list.
	 * @see model.ModelPackage#getGraphe_Sommet()
	 * @model containment="true"
	 * @generated
	 */
	EList<Sommet> getSommet();

	/**
	 * Returns the value of the '<em><b>Arc</b></em>' containment reference list.
	 * The list contents are of type {@link model.Arc}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arc</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arc</em>' containment reference list.
	 * @see model.ModelPackage#getGraphe_Arc()
	 * @model containment="true"
	 * @generated
	 */
	EList<Arc> getArc();

} // Graphe

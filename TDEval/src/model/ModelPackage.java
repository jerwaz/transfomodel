/**
 */
package model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see model.ModelFactory
 * @model kind="package"
 * @generated
 */
public interface ModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "modelURI";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "modelPrefix";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModelPackage eINSTANCE = model.impl.ModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link model.impl.GrapheImpl <em>Graphe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see model.impl.GrapheImpl
	 * @see model.impl.ModelPackageImpl#getGraphe()
	 * @generated
	 */
	int GRAPHE = 0;

	/**
	 * The feature id for the '<em><b>Sommet</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE__SOMMET = 0;

	/**
	 * The feature id for the '<em><b>Arc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE__ARC = 1;

	/**
	 * The number of structural features of the '<em>Graphe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Graphe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link model.impl.ArcImpl <em>Arc</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see model.impl.ArcImpl
	 * @see model.impl.ModelPackageImpl#getArc()
	 * @generated
	 */
	int ARC = 1;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC__RELATION = 0;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC__DESTINATION = 1;

	/**
	 * The feature id for the '<em><b>Origine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC__ORIGINE = 2;

	/**
	 * The number of structural features of the '<em>Arc</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Arc</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link model.impl.SommetImpl <em>Sommet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see model.impl.SommetImpl
	 * @see model.impl.ModelPackageImpl#getSommet()
	 * @generated
	 */
	int SOMMET = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOMMET__NAME = 0;

	/**
	 * The feature id for the '<em><b>Destinationed By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOMMET__DESTINATIONED_BY = 1;

	/**
	 * The feature id for the '<em><b>Origined By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOMMET__ORIGINED_BY = 2;

	/**
	 * The number of structural features of the '<em>Sommet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOMMET_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Sommet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOMMET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link model.Relation <em>Relation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see model.Relation
	 * @see model.impl.ModelPackageImpl#getRelation()
	 * @generated
	 */
	int RELATION = 3;


	/**
	 * Returns the meta object for class '{@link model.Graphe <em>Graphe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graphe</em>'.
	 * @see model.Graphe
	 * @generated
	 */
	EClass getGraphe();

	/**
	 * Returns the meta object for the containment reference list '{@link model.Graphe#getSommet <em>Sommet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sommet</em>'.
	 * @see model.Graphe#getSommet()
	 * @see #getGraphe()
	 * @generated
	 */
	EReference getGraphe_Sommet();

	/**
	 * Returns the meta object for the containment reference list '{@link model.Graphe#getArc <em>Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Arc</em>'.
	 * @see model.Graphe#getArc()
	 * @see #getGraphe()
	 * @generated
	 */
	EReference getGraphe_Arc();

	/**
	 * Returns the meta object for class '{@link model.Arc <em>Arc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arc</em>'.
	 * @see model.Arc
	 * @generated
	 */
	EClass getArc();

	/**
	 * Returns the meta object for the attribute '{@link model.Arc#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Relation</em>'.
	 * @see model.Arc#getRelation()
	 * @see #getArc()
	 * @generated
	 */
	EAttribute getArc_Relation();

	/**
	 * Returns the meta object for the reference '{@link model.Arc#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Destination</em>'.
	 * @see model.Arc#getDestination()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_Destination();

	/**
	 * Returns the meta object for the reference '{@link model.Arc#getOrigine <em>Origine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Origine</em>'.
	 * @see model.Arc#getOrigine()
	 * @see #getArc()
	 * @generated
	 */
	EReference getArc_Origine();

	/**
	 * Returns the meta object for class '{@link model.Sommet <em>Sommet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sommet</em>'.
	 * @see model.Sommet
	 * @generated
	 */
	EClass getSommet();

	/**
	 * Returns the meta object for the attribute '{@link model.Sommet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see model.Sommet#getName()
	 * @see #getSommet()
	 * @generated
	 */
	EAttribute getSommet_Name();

	/**
	 * Returns the meta object for the reference list '{@link model.Sommet#getDestinationedBy <em>Destinationed By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Destinationed By</em>'.
	 * @see model.Sommet#getDestinationedBy()
	 * @see #getSommet()
	 * @generated
	 */
	EReference getSommet_DestinationedBy();

	/**
	 * Returns the meta object for the reference list '{@link model.Sommet#getOriginedBy <em>Origined By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Origined By</em>'.
	 * @see model.Sommet#getOriginedBy()
	 * @see #getSommet()
	 * @generated
	 */
	EReference getSommet_OriginedBy();

	/**
	 * Returns the meta object for enum '{@link model.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Relation</em>'.
	 * @see model.Relation
	 * @generated
	 */
	EEnum getRelation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ModelFactory getModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link model.impl.GrapheImpl <em>Graphe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see model.impl.GrapheImpl
		 * @see model.impl.ModelPackageImpl#getGraphe()
		 * @generated
		 */
		EClass GRAPHE = eINSTANCE.getGraphe();

		/**
		 * The meta object literal for the '<em><b>Sommet</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPHE__SOMMET = eINSTANCE.getGraphe_Sommet();

		/**
		 * The meta object literal for the '<em><b>Arc</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPHE__ARC = eINSTANCE.getGraphe_Arc();

		/**
		 * The meta object literal for the '{@link model.impl.ArcImpl <em>Arc</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see model.impl.ArcImpl
		 * @see model.impl.ModelPackageImpl#getArc()
		 * @generated
		 */
		EClass ARC = eINSTANCE.getArc();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARC__RELATION = eINSTANCE.getArc_Relation();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARC__DESTINATION = eINSTANCE.getArc_Destination();

		/**
		 * The meta object literal for the '<em><b>Origine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARC__ORIGINE = eINSTANCE.getArc_Origine();

		/**
		 * The meta object literal for the '{@link model.impl.SommetImpl <em>Sommet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see model.impl.SommetImpl
		 * @see model.impl.ModelPackageImpl#getSommet()
		 * @generated
		 */
		EClass SOMMET = eINSTANCE.getSommet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOMMET__NAME = eINSTANCE.getSommet_Name();

		/**
		 * The meta object literal for the '<em><b>Destinationed By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOMMET__DESTINATIONED_BY = eINSTANCE.getSommet_DestinationedBy();

		/**
		 * The meta object literal for the '<em><b>Origined By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOMMET__ORIGINED_BY = eINSTANCE.getSommet_OriginedBy();

		/**
		 * The meta object literal for the '{@link model.Relation <em>Relation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see model.Relation
		 * @see model.impl.ModelPackageImpl#getRelation()
		 * @generated
		 */
		EEnum RELATION = eINSTANCE.getRelation();

	}

} //ModelPackage
